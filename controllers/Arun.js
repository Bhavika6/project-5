const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
// GET to this controller base URI (the default)
api.get('/', (req, res) => {
    res.render('Arun/index.ejs')
})
module.exports = api
